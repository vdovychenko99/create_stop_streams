#include <stdio.h>
#include <stdlib.h>

void kill(){
    pid_t pid;
    int status;
    printf("\n");
    while((pid=wait(&status))>0){
        printf("Zombie process with PID %d terminated with status %d\n",pid,status);
    }
    printf("\n");
}

void zombie(){
    pid_t pid;
    if ((pid=fork())==-1)
        exit(-1);
    if(pid==0){
        char* pr ="ps";
        char* args[]={"ps","uf","-C","zombie",NULL};
        execvp(pr,args);
        exit(-1);
    }else{
        int status;
        waitpid(pid,&status,NULL);
    }
}

int main() {
    pid_t pid;
    int i;
    for (i = 1; ;++i){
        if ((pid = fork()) == -1) {
            return -1;
        } else if (pid < 0) {
            if (i == 5) {
                zombie();
                kill();
                zombie();
                exit(0);
            }
        } else {
            printf("");
            exit(0);
        }
    }
    return 0;
}